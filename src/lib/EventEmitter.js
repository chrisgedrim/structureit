const EventEmitter = {
  events: {},
  dispatch: function(event, data) {
    if (this.events[event]) {
      this.events[event].forEach(handler => handler(data));
    }
  },
  on: function(event, handler) {
    this.events[event] = [...(this.events[event] || []), handler];
  }
};

export default EventEmitter;
