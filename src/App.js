import React, { useState } from 'react';
import { Container, Navbar, NavbarBrand } from 'reactstrap';
import EventEmitter from './lib/EventEmitter';
import NewUser from './components/NewUser';
import UserDetails from './components/UserDetails';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const App = () => {
  const [users, setUsers] = useState([]);

  EventEmitter.on('userAdded', user => setUsers([...users, user]));

  return (
    <div>
      <Navbar color='dark' dark className='d-flex justify-content-center mb-3'>
        <NavbarBrand tag='h1' className='mb-0'>
          StructureIT - Chris Gedrim
        </NavbarBrand>
      </Navbar>
      <Container>
        <div className='d-flex justify-content-end'>
          {users.length ? (
            <a href='#user-details'>Skip to user details</a>
          ) : (
            <a href='#add-new-user'>Skip to add new user</a>
          )}
        </div>
        <NewUser className='mb-3' />
        <UserDetails users={users} />
      </Container>
    </div>
  );
};

export default App;
