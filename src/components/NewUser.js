import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Button, Row, Col } from 'reactstrap';
import EventEmitter from '../lib/EventEmitter';

const NewUser = ({ id = 'add-new-user', ...props }) => {
  const defaultForm = {
    name: undefined,
    dateOfBirth: undefined,
    telephoneNumber: undefined,
    pet: undefined
  };
  const [form, setForm] = useState(defaultForm);

  const validPets = [undefined, 'Cat', 'Dog', 'Snake'];

  const onChange = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  const onSubmit = e => {
    e.preventDefault();

    EventEmitter.dispatch('userAdded', form);

    setForm(defaultForm);

    e.target.reset();
  };

  return (
    <Form onSubmit={onSubmit} {...props}>
      <h2 tabIndex='-1' id={id}>
        Add new user
      </h2>
      <Row tag='main' role='main'>
        <Col md='6'>
          <FormGroup>
            <Label for='name' id='nameLabel'>
              Name
            </Label>
            <Input
              type='text'
              name='name'
              id='name'
              defaultValue={form.name}
              onChange={onChange}
              required
              aria-required='true'
              autoComplete='name'
            />
          </FormGroup>
        </Col>
        <Col md='6'>
          <FormGroup>
            <Label for='dateOfBirth' id='dateOfBirthLabel'>
              Date of Birth
            </Label>
            <Input
              type='date'
              name='dateOfBirth'
              id='dateOfBirth'
              defaultValue={form.dateOfBirth}
              onChange={onChange}
              required
              aria-required='true'
              autoComplete='bday'
            />
          </FormGroup>
        </Col>
        <Col md='6'>
          <FormGroup>
            <Label for='telephoneNumber' id='telephoneNumberLabel'>
              Telephone Number
            </Label>
            <Input
              type='text'
              name='telephoneNumber'
              id='telephoneNumber'
              defaultValue={form.telephoneNumber}
              pattern='^\s*\(?(020[7,8]{1}\)?[ ]?[1-9]{1}[0-9{2}[ ]?[0-9]{4})|(0[1-8]{1}[0-9]{3}\)?[ ]?[1-9]{1}[0-9]{2}[ ]?[0-9]{3})\s*$'
              onChange={onChange}
              required
              aria-required='true'
              autoComplete='tel-national'
            />
          </FormGroup>
        </Col>
        <Col md='6'>
          <FormGroup>
            <Label for='pet' id='petLabel'>
              Favourite Pet
            </Label>
            <Input
              type='select'
              name='pet'
              id='pet'
              defaultValue={form.pet}
              onChange={onChange}
              required
              aria-required='true'
              autoComplete='off'
            >
              {validPets.map(pet => (
                <option
                  key={pet === undefined ? 'blank' : pet}
                  label={pet === undefined ? 'Please select an option' : pet}
                >
                  {pet}
                </option>
              ))}
            </Input>
          </FormGroup>
        </Col>
      </Row>
      <div className='d-flex justify-content-md-end'>
        <Button type='submit'>Add User</Button>
      </div>
    </Form>
  );
};

export default NewUser;
