import React from 'react';
import {
  Table,
  Card,
  CardBody,
  CardTitle,
  CardText,
  CardHeader,
  Col,
  Row
} from 'reactstrap';

const UserDetails = ({ users, id = 'user-details' }) => {
  if (!users.length) {
    return null;
  }

  return (
    <div role='presentation'>
      <h2 id={id} tabIndex='-1'>
        User details
      </h2>
      <Table className='d-none d-md-table'>
        <thead>
          <tr>
            <th>&nbsp;</th>
            <th>Name</th>
            <th>Date of Birth</th>
            <th>Telephone Number</th>
            <th>Favourite Pet</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, i) => (
            <tr key={i}>
              <th>{i}</th>
              <td aria-label={`name: ${user.name}`} tabIndex='-1'>
                {user.name}
              </td>
              <td
                aria-label={`date of birth: ${new Date(
                  user.dateOfBirth
                ).toLocaleDateString('default', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric'
                })}`}
                tabIndex='-1'
              >
                {new Date(user.dateOfBirth).toLocaleDateString()}
              </td>
              <td
                aria-label={`telephone number: ${user.telephoneNumber}`}
                tabIndex='-1'
              >
                {user.telephoneNumber}
              </td>
              <td aria-label={`favourite pet: ${user.pet}`} tabIndex='-1'>
                {user.pet}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {users.map((user, i) => (
        <Card key={i} className='d-md-none mb-3'>
          <CardHeader className='bg-dark'>
            <CardTitle
              className='mb-0 text-light'
              aria-label={`name: ${user.name}`}
              tabIndex='-1'
            >
              {user.name}
            </CardTitle>
          </CardHeader>
          <CardBody>
            <CardText tag='div'>
              <Row>
                <Col xs='12'>Date of Birth</Col>
                <Col
                  xs='12'
                  className='mb-2'
                  aria-label={`date of birth: ${new Date(
                    user.dateOfBirth
                  ).toLocaleDateString('default', {
                    day: 'numeric',
                    month: 'long',
                    year: 'numeric'
                  })}`}
                  tabIndex='-1'
                >
                  {new Date(user.dateOfBirth).toLocaleDateString()}
                </Col>
                <Col xs='12'>Telephone number</Col>
                <Col
                  xs='12'
                  className='mb-2'
                  aria-label={`telephone number: ${user.telephoneNumber}`}
                  tabIndex='-1'
                >
                  {user.telephoneNumber}
                </Col>
                <Col xs='12'>Favourite pet</Col>
                <Col
                  xs='12'
                  className='mb-2'
                  aria-label={`favourite pet: ${user.pet}`}
                  tabIndex='-1'
                >
                  {user.pet}
                </Col>
              </Row>
            </CardText>
          </CardBody>
        </Card>
      ))}
    </div>
  );
};

export default UserDetails;
